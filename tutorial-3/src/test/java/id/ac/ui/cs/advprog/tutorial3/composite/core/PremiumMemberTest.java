package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PremiumMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new PremiumMember("Wati", "Gold Merchant");
    }

    @Test
    public void testMethodGetName()
    {
        //TODO: Complete me
        assertEquals("Wati", member.getName());
    }

    @Test
    public void testMethodGetRole() {
        //TODO: Complete me
        assertEquals("Gold Merchant", member.getRole());
    }

    @Test
    public void testMethodAddChildMember() {
        //TODO: Complete me
        Member dummy = new OrdinaryMember("Dummy", "Dummy");
        member.addChildMember(dummy);
        assertEquals(1, member.getChildMembers().size());
    }

    @Test
    public void testMethodRemoveChildMember() {
        //TODO: Complete me
        Member dummy = new OrdinaryMember("Dummy", "Dummy");
        member.addChildMember(dummy);
        member.removeChildMember(dummy);
        assertEquals(0, member.getChildMembers().size());
    }

    @Test
    public void testNonGuildMasterCanNotAddChildMembersMoreThanThree() {
        //TODO: Complete me
        member.addChildMember(new OrdinaryMember("Dummy", "Dummy"));
        member.addChildMember(new OrdinaryMember("Dummy", "Dummy"));
        member.addChildMember(new OrdinaryMember("Dummy", "Dummy"));
        member.addChildMember(new OrdinaryMember("Dummy", "Dummy"));
        assertEquals(3, member.getChildMembers().size());
    }

    @Test
    public void testGuildMasterCanAddChildMembersMoreThanThree() {
        //TODO: Complete me
        Member master = new PremiumMember("Dummy", "Master");
        master.addChildMember(new OrdinaryMember("Dummy", "Dummy"));
        master.addChildMember(new OrdinaryMember("Dummy", "Dummy"));
        master.addChildMember(new OrdinaryMember("Dummy", "Dummy"));
        master.addChildMember(new OrdinaryMember("Dummy", "Dummy"));
        assertEquals(4, master.getChildMembers().size());
    }
}
