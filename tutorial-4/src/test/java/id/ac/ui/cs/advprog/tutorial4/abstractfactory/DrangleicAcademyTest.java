package id.ac.ui.cs.advprog.tutorial4.abstractfactory;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.DrangleicAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.KnightAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.DrangleicArmory;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MajesticKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.Knight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MetalClusterKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.SyntheticKnight;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class DrangleicAcademyTest {
    KnightAcademy drangleicAcademy;
    Knight majesticKnight;
    Knight metalClusterKnight;
    Knight syntheticKnight;

    @BeforeEach
    public void setUp() {
        // TODO setup me
        drangleicAcademy = new DrangleicAcademy();
        majesticKnight = drangleicAcademy.getKnight("majestic");
        metalClusterKnight = drangleicAcademy.getKnight("metal cluster");
        syntheticKnight = drangleicAcademy.getKnight("synthetic");
    }

    @Test
    public void checkKnightInstances() {
        // TODO create test
        assertTrue(majesticKnight instanceof Knight);
        assertTrue(metalClusterKnight instanceof Knight);
        assertTrue(syntheticKnight instanceof Knight);
    }

    @Test
    public void checkKnightNames() {
        // TODO create test
        majesticKnight.setName("Majestic");
        metalClusterKnight.setName("Metal");
        syntheticKnight.setName("Synthetic");
        assertEquals("Majestic", majesticKnight.getName());
        assertEquals("Metal", metalClusterKnight.getName());
        assertEquals("Synthetic", syntheticKnight.getName());
    }

    @Test
    public void checkKnightDescriptions() {
        // TODO create test
        assertEquals("Shining Buster", majesticKnight.getWeapon().getName());
        assertEquals("Metal Armor", majesticKnight.getArmor().getName());

        assertEquals("Metal Armor", metalClusterKnight.getArmor().getName());
        assertEquals("Shining Force", metalClusterKnight.getSkill().getName());

        assertEquals("Shining Buster", syntheticKnight.getWeapon().getName());
        assertEquals("Shining Force", syntheticKnight.getSkill().getName());
    }



}
