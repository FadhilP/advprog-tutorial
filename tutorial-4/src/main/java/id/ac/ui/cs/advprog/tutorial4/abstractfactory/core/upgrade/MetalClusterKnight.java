package id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.Armory;

public class MetalClusterKnight extends Knight {

    public MetalClusterKnight(Armory armory) {
        this.name = "Metal Cluster";
        this.armory = armory;
    }

    @Override
    public void prepare() {
        skill = armory.learnSkill();
        armor = armory.craftArmor();
        // TODO complete me
    }
}
