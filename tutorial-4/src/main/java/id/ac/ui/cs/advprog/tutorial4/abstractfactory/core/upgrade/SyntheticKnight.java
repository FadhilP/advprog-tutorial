package id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.Armory;

public class SyntheticKnight extends Knight {

    public SyntheticKnight(Armory armory) {
        this.name = "Synthetic";
        this.armory = armory;
    }

    @Override
    public void prepare() {
        weapon = armory.craftWeapon();
        skill =  armory.learnSkill();
        // TODO complete me
    }
}
